# TODO

## Just generally

- rescale to UPM 4000 (see Brill/Hudson)
- review all forms made with stroking, and convert to outline
- Do I need /Dz.sc ? and if so, shouldn't it have different
  behaviour for each of `smcp` `c2sc` applied to /Dz ?
  (yes, but do we really apply `c2sc` and not `smcp`?)
- Check/balance dotabove with dieresis for capitals
- consider compact /I_J liga; like https://fontsinuse.com/uses/53136/trambanen-vrijhouden-poster
- what should +case +smcp do? what about +case +c2sc
- for ï compact diacritics
- /n has advance width 604; consider 600 instead?
- review side-bearings on `(` and `)` which are loose compared
  to `[]`
- Consider cap-height ballot boxes
- Less weight on horizontal strokes of /I ?
- consider /periodcentered.case; current · is more harmonised
  with capitals
- make /k more cognate with uc
- slashed /zero (see Intel One Mono, which has a slash that is
  more horizontal than usual; therefore does not conflict with Ø
  so much)
- [done] fix /notgreater and so on, again?

## Kerning

- ī (/imacron) etc
- /dcaron /quotedblright
- [EGHIJMV] + (/idieresis | /idieresis.sc)
- /slash + (/idieresis | /igrave)
- check /D/A

# END
