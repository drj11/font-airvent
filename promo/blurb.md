# Airvent

Blurb for futurefonts

## About

A "tracing" of a very poor found specimen from 1968
yielded a basic A to Z plus digits for a plain monoline
sans-serif with rounded terminals.

I've added a few more latin letters, and a small amount of
punctuation and symbols.

The simplicity appeals to me,
partly because it feels like a moldable putty.
It can be played with and pulled into new shapes and experiments.

I already like using it for basic utility work (sign, captions,
headlines).

## Future

As much as possible.
I particularly think of it as a matrix in which to try
experiments that i might not try in other fonts:
- 3/4 height numbers
- 3/4 height caps
- loose and tight tracking settings for capitals
- vertically centered capitals
- coherent families with different styles (sans, slab, flare, marker).

Some of these are detailed and shown in the PDF.

## About Me

In 2020 in response to the long year of the pandemic i switched
from being a programmer to being a type designer.
In March 2021 i bought a second-hand Mac Book Pro and took a
one-day course with The League of Movable Type using Glyphs Mini.

In August 2021 Atwin, my first commercial font,
was listed for retail on myfonts.com.

# END
