# Airvent

A "tracing" of USA government Military Standard 33558(ASG)
«Numeral and Letters, Aircraft Instrument Dial, Standard Form
of», Revision C issued in 1968.
Extended into a more complete font.

![airvent](airvent.svg)

The available scan is a hilarious dotty, grainy, low-res 1-bit
scan.

Entirely new:

- **Æ** and other non-ASCII letters
- **&** and other symbols
- accents and diacritics
- punctuation: . , : ; / \ [ ] - « »
- fractions both pre-composed and via `frac` feature
- inferior/superior numbers
- smallcaps, including smallcaps numbers
- lowercase


## Design metrics

Nominal stroke thickness 120 for capitals, 100 for smallcaps and lowercase.

- Capital height is 800.
- 8 units overshoot for narrow projections, like A and V
  (previously 16 to 20).
- 6 units overshoot for broader projections, like O (previously
  10 or 12).
- 0 overshoot for straight segments, like E Z.

- x-height 560
- ascender height 860
- descender height -300
- 8/6 overshoots (for sharp/round)

- smallcap (letters and numbers) at 600,
  almost a half stroke above the x-height.
- overshoots 8/6

2023 is a year of change for vertical metrics,
mostly as a result of adding an entirely new lower case.
The median lines moved up, raising the smallcap E height from
552 to 600 (6% of /E height).
Overshoots were also decreased from 16/12 to 8/6.

A set of smol numbers are drawn.

- small number topline 500
- `.smol` are the component drawings that are re-used
- shifted vertically for inferior (`subs` and `sinf` feature),
  superior (`sups`), fractions (`dnom` and `numr`)
- fractions are aligned with capitals;
  they do not project above or below
- inferior and superior project by 100.
- overshoots at 12 (may need to be reduced).
- Monospaced at 480 advance width.

Q has a slightly less wide oval than O.

R may require some more cleanup (stroke width of median
horizontal).

Asterisk size and design is somewhat draft.
Asterisk vertical position is entirely arbitrary
(set at exactly 50% cap-height)


## Notes and Queries

To keep the font consistent,
a few glyphs have been redrawn from their specimen versions.
**O** **Q** **6** and **8** because they are too heavy.
**4** because it is too wide.
Versions that are closer to the original scan are in glyphs that
end in `.scan`.

The dieresis on ü, should it be exactly over the vertical stems?
Should that dieresis spacing be used for the other letters?

- Q: Should **D** have rounded corners? (Also **E** **F** and so on)
- A: a design feature is that some of the capitals are allowed square
  or sharp corners.

The specimen form of **1**, which is a vertical stroke,
is not well suited to fixed width (tabular).
The primary **1** has a flag, with `/one.scan` available
for the drawing closer to the scan.

Guillemets align with mid cap-height and hyphen, because
they were originally drawn for all-caps.

- Q: should tapered forms (for example `\quotesingle` `\exclam` `\caroncomb`)
  be made mono?
- A: no, it's too ugly and hard.


## Future

The simple monoline design lends itself to development across a
series of families and styles.
The font is rapidly rising within Cubic Type as a general
utility font.

There are many developments planned for this basic design.
At the head of the queue:

- spacing and kerning improvements
  (a draft for capitals and smallcaps is done)
- character variants:  0 with slash
- extended latin accents
- triline
- 5 layer
- 3/4 height numbers
- small caps accessories (dashes and so on)

Triline designs follow in the tradition of
[Mexico 68](https://fontsinuse.com/typefaces/36706/mexico-olympic),
[Embrionic Triline](https://fontsinuse.com/typefaces/32190/embrionic-triline),
[Prisma](https://fontsinuse.com/typefaces/7413/prisma),
[Pump Triline](https://fontsinuse.com/typefaces/7980/pump-triline).

Surprisingly it seems there is still potential for interesting
developments: few existing triline forms have the outer stroke
forming an outline, as shown in the examples.

More conventional developments such as bold,
oblique, cyrillic, and so on are also planned.
And then developments for stroke constrast,
square-cut terminals, flared calligraphic terminals,
and marker-pen styles.

Other future plans:

- proportional numbers
- 3/4 caps
- lower case numbers
- bold and oblique
- stroke width 1/6 (17%) and 1/8 (12%), (currently 15%)
- TnT and Capital Headings track (for spacing)
- mathematical operators
- cyrillic
- cameo (in boxes)
- contrast
- Terminals: flare / butt / marker-pen


## Names

- Airvent
- Air1968 [original pre-production name]
- Airframe
- Force [taken: https://fontsinuse.com/typefaces/19394/force]
- Altimeter
- Airspeed
- Avionic [taken: https://fontsinuse.com/typefaces/119756/avionic]
- Aidform

A variant of this font that is mostly a subset is used for the `feature-zone`;
it's called _Airtest_.


## References

[TRICKS] Sofie Beier, «Type Tricks», 2017, Amsterdam.


# END
