# Rescaling from UPM 1200 to 4000, Preliminary Report

You can change the UPM from one arbitrary value to another,
simply edit its value in the text field. This is not about that.

Pressing the rescale button next to the field _rescales_ the
font to the new UPM.
All the drawings are scaled by the appropriate scale factor.
How does this work out?

## Executive Summary for Policymakers

On a mature font, plan 1 to 4 days rework and evaluation.

For a family, plan as if for a complete rework.


## Problem Statement

Problems can be divided into 2 classes:

- stroked paths do not scale;
- advance width does not (exactly) sum to LSB + span + RSB

The problem with stroked paths is completely straightforward
(and probably a bug fix in Glyphs):
The stroke widths do not scale. So a stroke width of 100, when
the UPM is 1000, will be 10% of the UPM. When the font is scaled
to 4000 UPM, the stroke width is _still_ 100, which is now 2.5%
of the UPM. *sad trombone*

The advance width problem is a mixture of things:
- Arithmetic roundoff.
- computed LSB/RSB/advance width metrics

for example my /AE has:
- LSB =A (167); RSB =E (167)
- horizontal extent of drawing 3443
- Advancw Width 3777

In this case those numbers add up to 3777, but the problem is
that =E is 166 so this gyphs shows as Metrics out of Sync.
Using Update Metrics nudges the RSB back to 166 and adjusts the
Advance Width.

Similar problems with /Eth /Ezh and so on.

Another sort of computation is more serious. My /a has an RSB
of: =n@300 (127). This is way off for 2 reasons:

- the calculated 127 is way off, which is related to...
- the @300 should have been scaled (to @1000 in this case).

For Airvent there are approximately 38 glyphs with incorrect
metrics, and probably a similar number using incorrectly stroked
paths.

## END
