# Spacing

- [x] CAP spaced
- [x] CAP×CAP kerned
- [x] SMALLCAP spaced
- [x] SMALLCAP×SMALLCAP kerned
- [x] SMALLCAP×CAP kerned
- [x] LC spaced
- [x] LC×LC kerned
- [ ] CAP×LC kerned
- [ ] SMALLCAP×LC kerned?
- [ ] numbers?
- [ ] letter×punctuation?


## Kerning

## CAP×LC

- Ay Aw Ay vA wA yA -50
- F/idotless -40
- T /@a T /@g T /@o -30
- V /@a V /@g V /@o -30
- VsV -20
- VxV -10
- mV nV -10
- W /@a W /@g W /@o -30
- WsW -20
- WxW -10
- mW nW -10
- Y /@a Y /@g Y /@o -50
- aY cY -20
- /@o Y eY -30
- @n Y -10
- YrY -20
- YsY -20
- YxY -20
- cV cW -20
- cT eT -20
- nT mT -10
- kT -30
- kV kW -20

- [ ] /Eth ?
- [ ] /f on right
- [ ] Gï

## LC×LC

- /dcaron [bhklþ] +60
- /dcaron ß +40
- ox xo -20


## CAP×CAP

1st pass on CAPxCAP complete.

- CA seems fine
- FA -60
- AJ seems fine
- AT TA -50
- AC -20
- JA seems fine
- ÞA -50
- AV VA -50
- AO AQ grouped with AC
- PA -50
- AW WA -40
- AØ fine
- AY YA -60

- YC -40
- DÆ -50
- DY -50

- FÆ -100 (could be even more)
- FJ fine

- LT -30 (note L has a small RSB)
- LV LW -20
- LY -70

- PA fine
- PÆ -120

- TA DT fine
- TÆ -50
- ÞT fine
- ÞÆ -70
- ÞY -40

- VÆ -70
- WÆ -50
- YÆ -70


## Upper case

Spacing for A to Z using [TRICKS] as a guide.

Set H to 100 H 100.

Set O to 75 H 75.

Set M N to 100 M 100.

Set B C D E to 100 B 50, 75 C 50, 100 D 75, 88 E 50.

Set F as E; and G as C.

Set I U as H.

Set J to 10 J 100.

Set K as E.

Set L to 100 L 10.

Set P R as D.

Set Q as O (tails counts for nothing).

Set A V W X Y Z to 50 A 50. (no change for W X Y Z)

Set S as O.

Set T to 25 T 25 (the tightest).


## Small Caps 552

h: 80 h 80

o: 60 o 60

m n: 80 m 80

b d e f: 80 b 40, 80 e 40, f as e, k as e

d: 80 d 60, p as d, r as d, thorn as p

c g: 60 c 40, g as c

i u: as h

j: 10 j 80

l: 100 l 10

q: as o (not counting tail)

a: 40 a 40, v as a, w x y z as a

s: as o

t: 20 t 20


## Small Caps 552 kerning

- ac ao aq -20 (left-group [coq])
- da ða oa (right-group [d ð o]) -20
- fa -60
- oa øa -20
- pa at ta -50
- þa -40
- av va aw wa -50
- ay ya -60

- dæ -30
- dt dv dw dx dy -20

- fæ -100
- fj fo fø unchanged

- lt lv lw -30
- ly -60

- pæ -100

- tæ þæ -60
- þv þw þx þy -20

- væ wæ yæ -60


## Cap × small-cap kerning

- fA At tA Av vA Aw wA -50
- þA pA yA Ay -50
- Da Dæ -50
- aC (=aO) -50
- Fa Fæ Pæ -80
- Pa -50
- Tx Ty Tz Tr Ts Tt Tu T̈ü Tv -50 (and symmetrically xT yT and so on)
- There may be some missing Ta and so on here
- Va Væ aV -50
- Vo (= Vc) oV -30
- Vx lV -20
- pV -20
- þV -30

- Wa aW wæ -30
- þW pW -20
- xW -10

- Ya aY Yæ -50
- Yc Yo Yq -30
- lY -20
- pY þY -30

- Kv Kw Ky -50
- sV sW Vs Ws -20
- sY Ys -40
- Jæ Uæ -30
- Yq qY -30
- Vß -40
- Wo oW -30
- rY -20
- Þa -20
- Þæ -40


# END
